package Lesson10;

public class Square  {

    private double side;
    private int grad;
    private int point;

    public Square(double side, int grad, int point) {
        this.side = side;
        this.grad = grad;
        this.point = point;
    }

    public double area() {
        return side * side;
    }


    public void getArea() {

        System.out.println("Площадь квадрата равна " + area());
    }


    public void moveFigure() {
        if (point >0) {
            System.out.println("Фигура была подвинута вправо по оси Х на " + point);}
        else System.out.println("Фигура была подвинута влево по оси Х на  "  + point);
    }

    public void rotateFigure() {
        System.out.println("Фигура была повернута на " + grad + " градусов");
    }
}
