package Lesson10;


public class Rectangle  {

    private double width ;
    private double height ;

    public Rectangle(double width, double height, int point, int grad) {
        this.width = width;
        this.height = height;
        this.point = point;
        this.grad = grad;
    }

    private int point ;
    private int grad;

    public double area() {
        return width * height;
    }


    public void getArea() {

        System.out.println("Площадь прямоугольника равна " + area());
    }

    public void moveFigure() {
        if (point >0) {
        System.out.println("Фигура была подвинута вправо по оси Х на " + point);}
        else System.out.println("Фигура была подвинута влево по оси Х на  "  + point);
    }


    public void rotateFigure() {
        System.out.println("Фигура была повернута на " + grad + " градусов");
    }

}