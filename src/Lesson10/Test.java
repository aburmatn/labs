package Lesson10;


public class Test {

    public static void main(String[] args) {


        Double r = Double.valueOf(3);
        Integer grad = Integer.valueOf(5);
        Integer point = Integer.valueOf(7);

        //Здесь компилятор говорит про "ненужный боксинг"

        Circle circle = new Circle(r,grad,point);

        Double width = new Double(5.5);
        Double height = new Double(7.5);
        Integer pointRecht = new Integer(5);
        Integer gradRecht = new Integer(10);

        Rectangle rectangle =new Rectangle(width,height,pointRecht,gradRecht);

        Double side = new Double(7.1);
        Integer gradSq = new Integer(5);
        Integer pointSq = new Integer(6);

        Square square = new Square(side,gradSq,pointSq);

        circle.getArea();
        circle.moveFigure();
        circle.rotateFigure();

        rectangle.getArea();
        rectangle.moveFigure();
        rectangle.rotateFigure();

        square.getArea();
        square.moveFigure();
        square.rotateFigure();

    }
}

