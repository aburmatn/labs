package Lesson10;

public class Circle {

    private double r;
    private int grad;
    private int point;


    Circle(double r, int grad, int point){
        this.r = r;
        this.grad = grad;
        this.point = point;

    }


    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public int getGrad() {
        return grad;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }


    public double area() {
        return Math.PI * r * r;
    }


    public void getArea() {

        System.out.println("Площадь круга равна " + area());
    }

    public void moveFigure() {
        if (point >0) {
            System.out.println("Фигура была подвинута вправо по оси Х на " + point);}
        else System.out.println("Фигура была подвинута влево по оси Х на  "  + point);
    }

    public void rotateFigure() {
        System.out.println("Фигура была повернута на " + grad + " градусов. Зачем вы вращаете круг?");
    }
}