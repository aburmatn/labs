package lesson23;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyFile {

    public static void main(String[] args) {
        File file1 = new File("C:\\Java\\BS\\labs\\src\\lesson23\\1.txt");
        File file2 = new File("C:\\Java\\BS\\labs\\src\\lesson23\\2.txt");

        try (FileWriter fileWriter = new FileWriter(file1,false)) { // try with recources - JVM закроет поток, если что-то пойдет не так. Не надо зкрывать в finally
            fileWriter.write("Содержимое этого файла будет скопированно в файл 2.");
            fileWriter.append('\n');
            fileWriter.flush(); // метод, который все скидывает в файл
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileReader fr = new FileReader(file1);
             FileWriter fw = new FileWriter(file2)) {
            int c = fr.read();
            while(c!=-1) {
                fw.write(c);
                c = fr.read();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}

