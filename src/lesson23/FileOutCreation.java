package lesson23;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileOutCreation {

    public static void main(String[] args) {
        File file = new File("C:\\Java\\BS\\labs\\src\\lesson23\\out.txt");

        try (FileWriter fileWriter = new FileWriter(file,false)) { // try with recources - JVM закроет поток, если что-то пойдет не так. Не надо зкрывать в finally
            fileWriter.write("Это");
            fileWriter.append('\n');
            fileWriter.write("Файл");
            fileWriter.append('\n');
            fileWriter.write("Записанный");
            fileWriter.append('\n');
            fileWriter.write("Программой на JAVA!");
            fileWriter.append('\n');
            fileWriter.flush(); // метод, который все скидывает в файл
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileWriter fileWriter = new FileWriter(file,true)) { // try with recources - JVM закроет поток, если что-то пойдет не так. Не надо зкрывать в finally
            fileWriter.write("Написанной студентом Бурматновой Анной");
            fileWriter.append('\n');
            fileWriter.flush(); // метод, который все скидывает в файл
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
