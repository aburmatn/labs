package lesson23;

import java.io.File;
public class ShowTree {
    public static void main(String[] args) {

        File dir = new File("C:\\Java\\BS\\labs\\ShowTreeTestFolder");
        if (dir.isDirectory()) {
            for (File item : dir.listFiles()) {
                if (item.isDirectory()) {
                    System.out.println("d: " + item.getName() + "\t folder");
                } else {
                    System.out.println(item.getName() + "\t file");
                }
            }
        }

    }
}


