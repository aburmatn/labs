package polzovateli;

import Produkty.Produkt;
import banky.Banka;

public class Polzovatel {
    String nameP;

    public Polzovatel(String name) {
        this.nameP = name;
    }

    public void eat (Banka banka) {
        Produkt produkt = banka.takeProdukt();
        System.out.println(this.nameP + " says: Ya s'el " + produkt.name + ". Eto bylo vkusno! Yummy!");

    }
}
