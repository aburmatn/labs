package banky;

import Produkty.Produkt;
import Produkty.dary_lesa.Grib;

public class Banka {
    Produkt produkt;

    public Banka(Produkt produkt) {
        this.produkt = produkt;
        System.out.println("New banka! V banke: " + produkt.name + "!");
    }

    public Produkt takeProdukt() {
        System.out.println("Iz banky dostali produkt - " + produkt.name + "!");
        return produkt;
    }

}
