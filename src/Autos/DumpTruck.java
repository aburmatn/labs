package Autos;

public class DumpTruck extends Truck {


    public DumpTruck(int enginePower, int mass,int truckVolume) {
        super(enginePower, mass);
        this.truckVolume = truckVolume;
    }

    public void upTruckbed(){
        System.out.println("Кузов самосвала поднят");
    }

    public void setTruckbedDown(){
        System.out.println("Кузов самосвала опущен");
    }

    @Override
    public void loadTruckUp(int truckVolume) {
        super.loadTruckUp(truckVolume);
    }

    @Override
    public void dischargeTruck(int truckVolume) {
        super.dischargeTruck(truckVolume);
    }
}
