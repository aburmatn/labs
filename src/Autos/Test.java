package Autos;

public class Test {

    public static void main(String[] args) {
        PassengerСar ladaSedan = new PassengerСar(300,1000,4);
        Truck truck1 = new Truck(500,3000,900);
        DumpTruck dumpTruck1 = new DumpTruck(500,2500,1000);

        ladaSedan.pickUpPassangers(ladaSedan.amountOfPassangers);
        ladaSedan.dropPassengers(ladaSedan.amountOfPassangers);

        truck1.loadTruckUp(truck1.getTruckVolume());
        truck1.dischargeTruck(truck1.getTruckVolume());

        dumpTruck1.upTruckbed();
        dumpTruck1.loadTruckUp(dumpTruck1.getTruckVolume());
        dumpTruck1.setTruckbedDown();
        truck1.dischargeTruck(dumpTruck1.getTruckVolume());

    }
}
