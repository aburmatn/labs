package Autos;

public class Car {
    private int enginePower;
    private int mass;

    public Car(int enginePower, int mass) {
        this.enginePower = enginePower;
        this.mass = mass;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    public Car() {
    }
}

