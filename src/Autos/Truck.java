package Autos;

public class Truck extends Car{

    int maxTruckBedVolume = 1000;
    protected int truckVolume;

    public Truck(int enginePower, int mass,int truckVolume) {
        super(enginePower, mass);
        this.truckVolume = truckVolume;
    }


    public Truck(int maxTruckBedVolume, int truckVolume) {
        this.maxTruckBedVolume = maxTruckBedVolume;
        this.truckVolume = truckVolume;
    }

    public int getTruckVolume() {
        return truckVolume;
    }

    public void setTruckVolume(int truckVolume) {
        this.truckVolume = truckVolume;
    }

    public void loadTruckUp(int truckVolume){
        if (truckVolume<=maxTruckBedVolume){
        System.out.println("Кузов загружен " + truckVolume + " кг");}

        else System.out.println("Вы пытаетесь нагрузить кузов больше, чем его максимальный объем. Имейте совесть!");
    }

    public void dischargeTruck(int truckVolume){
        if (truckVolume<=maxTruckBedVolume)
        System.out.println("Из кузова выгружен груз массой " + truckVolume + " кг ");
        else System.out.println("Погрузки не было. Ничего не доступно к выгрузке");
    }
}
