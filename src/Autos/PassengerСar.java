package Autos;

public class PassengerСar extends Car {

    protected int amountOfPassangers;
    protected int maxAmountOfPassangers = 4;


    public PassengerСar(int enginePower, int mass,int amountOfPassangers) {
        super(enginePower, mass);
        this.amountOfPassangers = amountOfPassangers;
    }

    public void dropPassengers(int amountOfPassangers) {
        if (amountOfPassangers == 0) {
            System.out.println("Все пассажиры ушли. Некого высаживать");
        } else
            System.out.println("Пасажиры высажены из автомобиля.");

    }

    public void pickUpPassangers(int amountOfPassangers) {
        if (amountOfPassangers <= maxAmountOfPassangers) {
            System.out.println( amountOfPassangers + " пасажира сели в автомобиль");
        } else
            System.out.println("Вы не можете перевозить больше пассажиров, чем это требует техника безопасности. Скиньте балласт ;)");


    }

}