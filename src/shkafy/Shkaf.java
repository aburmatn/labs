package shkafy;

import banky.Banka;

public class Shkaf {
    int countBanky;
    int maxCountBanky;

    public Shkaf (int maxCountBanky) {
        this.maxCountBanky = maxCountBanky;
        countBanky =0;
        System.out.println("Novii shkaf vmestimostyu " + maxCountBanky + " b. Shkaf pustoy.");
    }

    public void pullBanka(Banka banka) {
        if (maxCountBanky > countBanky) {
            countBanky++;
            System.out.println("Vy polozhili banku v shkaf. Teper kolichestvo banok v shkafu - " + countBanky + ".");
        }
        else {
            System.out.println("V shkafu net mesta!");
        }
    }

    public void takeBanka(Banka banka) {
        if (countBanky > 0) {
            countBanky--;
            System.out.println("Vy vzyali banku iz shkafa. Teper kolichestvo banok v shkafu - " + countBanky + ".");
        }
        else {
            System.out.println("V shkafu pusto!");
        }
    }
}
