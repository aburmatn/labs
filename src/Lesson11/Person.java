package Lesson11;

public class Person {
    final String name;
    String middleName;
    String lastName;
    int year;
    private Address homeAddress;
    String wife;
    String child;

    Person(String name,String middleName,String lastName, int year,String wife,String child, Address address){

        this.name = name;
        this.middleName = middleName;
        this.lastName = lastName;
        this.year = year;
        this.wife = wife;
        this.child = child;
        this.homeAddress = address;
    }

    Person(String name){
        this.name = name;
    }



    public String getAdress(){
        return homeAddress.country + ",  " + homeAddress.city+ ",  " + homeAddress.street+ ", "+ homeAddress.plz;
    }

    public void setAddress(String country, String city, String street,int bd, int apt, int plz){
        homeAddress.country = country;
        homeAddress.city = city;
        homeAddress.street = street;
        homeAddress.bd = bd;
        homeAddress.apt = apt;
        homeAddress.plz = plz;

    }

}
