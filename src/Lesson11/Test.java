package Lesson11;

public class Test {

    public static void main(String[] args) {

        Address address1 = new Address("Россия", "Санкт-Петербург", "Каменноостровкий",15,5, 194355);
        System.out.println("Указанный адрес " + address1.country + ", " + address1.city + ", " + address1.street + ", " + address1.plz);

        Address address2 = new Address("Россия", "Иркутск", "Ленина", 10,68);
        System.out.println("Указанный адрес " + address2.country + ", " + address2.city + ", " + address2.street);



//   Family 1
        Address olegAddress = new Address("Россия", "Санкт-Петербург", "Пушкина",17, 6, 198520);
        Person oleg = new Person("Олег", "Петрович", "Кривоусов", 1973, "Наталья"," Степан",olegAddress);
        System.out.println("Информация о запрашиваемом человеке ");
        System.out.println(oleg.name + " " + oleg.middleName  + " " + oleg.lastName+ " " + oleg.year+ ", жена " + oleg.wife+ ",  дети " + oleg.child);
        System.out.println("Семья " + oleg.lastName + " проживает по адресу ");
        System.out.println(oleg.getAdress());


    //   Family 2
         Address michailAddress = new Address("Россия", "Москва", "Ленина",1, 1, 1111111);
        Person michail = new Person("Михаил", "Сергеевич", "Потапов", 1960, "Мария"," Анастасия",michailAddress);
        System.out.println("Информация о запрашиваемом человеке ");
        System.out.println(michail.name + " " + michail.middleName  + " " + michail.lastName+ " " + michail.year+ ", жена " + michail.wife+ ",  дети " + michail.child);
        System.out.println("Семья " + michail.lastName + " проживает по адресу ");
        System.out.println(michail.getAdress());
}

}
