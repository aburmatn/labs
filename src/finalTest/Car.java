package finalTest;

public class Car extends MotorVehicle implements CarChoise{

    private String airConditioner; //наличие кондиционера

    public Car(int pricePerMinute, String model, String type, String airConditioner) {
        super(pricePerMinute, model, type);
        this.airConditioner = airConditioner;
    }

    public String getAirConditioner() {
        return airConditioner;
    }

    public void setAirСonditioner(String airConditioner) {
        this.airConditioner = airConditioner;
    }


    @Override
    public void selectCar() {
        System.out.println(getModel() + " " + getPricePerMinute()+  "р в минуту "  +  " " + getAirConditioner());
         }
}
