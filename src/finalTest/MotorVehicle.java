package finalTest;

public class MotorVehicle {

    private int pricePerMinute;
    private String model;
    private String type;

    public MotorVehicle(int pricePerMinute, String model,String type ) {
        this.pricePerMinute = pricePerMinute;
        this.model = model;
        this.type = type;
    }

    public int getPricePerMinute() {
        return pricePerMinute;
    }

    public void setPricePerMinute(int pricePerMinute) {
        this.pricePerMinute = pricePerMinute;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
