package finalTest;

public class Renter {

    private String driverLicenseType;

    public Renter( String driverLicenseType) {
        this.driverLicenseType = driverLicenseType;
    }


    public String getDriverLicenseType() {
        return driverLicenseType;
    }

    public void setDriverLicenseType(String driverLicenseType) {
        this.driverLicenseType = driverLicenseType;
    }

}