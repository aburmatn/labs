package finalTest;

public class Test {
    public static void main(String[] args) {

        Renter renter1 = new Renter("A");
        Renter renter2 = new Renter("AB");
        Renter renter3 = new Renter("B");

        Car Lada = new Car(3, "Лада Седан", "A", "Без кондиционера");
        Car Mazda = new Car(3, "Мазда", "A", "С кондиционером");

        Bike Ural = new Bike(2, "Урал Волк", "B", "С коляской");
        Bike Honda = new Bike(5, "Хонда", "B", "Без коляски");
        Bike Suzuki = new Bike(4, "Suzuki", "B", "Без коляски");

        CarChoise[] allowedBikes = new CarChoise[3];
        allowedBikes[0] = Ural;
        allowedBikes[1] = Honda;
        allowedBikes[2] = Suzuki;

        CarChoise[] allowedCars = new CarChoise[2];

        allowedCars[0] = Mazda;
        allowedCars[1] = Lada;


        if (renter1.getDriverLicenseType().equalsIgnoreCase("B")) {
            System.out.println("Арендатору доступны следующие модели транспортных средств: Автомобили и мотоциклы");
            for (int i = 0; i < allowedBikes.length; i++) {
                allowedBikes[i].selectCar();

            }
        } else if ((renter1.getDriverLicenseType().equalsIgnoreCase("A"))) {
            System.out.println("Арендатору доступны следующие модели транспортных средств: Автомобили");
            for (int i = 0; i < allowedCars.length; i++) {
                allowedCars[i].selectCar();
            }
        } else if (renter1.getDriverLicenseType().equalsIgnoreCase("AB")) {
            System.out.println("Арендатору доступны следующие модели транспортных средств: Автомобили");
            for (int i = 0; i < allowedCars.length; i++) {
                allowedCars[i].selectCar();
            }

            System.out.println("Арендатору доступны следующие модели транспортных средств: Мотоциклы");
            for (int j = 0; j < allowedBikes.length; j++) {
                allowedBikes[j].selectCar();
            }


        }


    }
}
