package finalTest;

public class Bike extends MotorVehicle implements CarChoise {

    private String isSoloBike;

    public Bike(int pricePerMinute, String model, String type, String isSoloBike) {
        super(pricePerMinute, model, type);
        this.isSoloBike = isSoloBike;
    }

    public String isSoloBike() {
        return isSoloBike;
    }

    public void setSoloBike(String soloBike) {
        isSoloBike = soloBike;
    }

    public void showBikes(){
        System.out.println(getModel() + " " + getPricePerMinute() + " " + isSoloBike );
    }



    @Override
    public void selectCar() {
            System.out.println(getModel() + " " + getPricePerMinute()+  "р в минуту "  +  " " + isSoloBike);


    }
}
