package lesson19;

public class Test {

    public static void main(String[] args) {
        Actions package1 = new Package();

        StuffToPut book = new StuffToPut(12,"книга");
        StuffToPut pen = new StuffToPut(2,"ручка");
        StuffToPut jacket = new StuffToPut(21,"куртка");

        package1.put(pen.getStuffToPutVolume(),pen.getName());
        package1.put(book.getStuffToPutVolume(),book.getName());
        package1.put(jacket.getStuffToPutVolume(),jacket.getName());


    }

}
