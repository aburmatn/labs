package lesson19;

public class StuffToPut {
    private int stuffToPutVolume;
    private String name;

    public StuffToPut(int stuffToPutVolume, String name) {
        this.stuffToPutVolume = stuffToPutVolume;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getStuffToPutVolume() {
        return stuffToPutVolume;
    }



}
