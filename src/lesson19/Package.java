package lesson19;

public class Package implements Actions  {

    private static int volume = 20;
    private  int leftSpace;

    public int getleftSpace() {
        return leftSpace;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    @Override
    public void put(int stuffToPutVolume,String name) {
        System.out.println("Проверяем, можете ли вы положить выбранный предмет '" + name+ "'  в пакет ");
        leftSpace = volume-stuffToPutVolume;
        if  (volume>stuffToPutVolume)
        System.out.println("Вы положили в пакет предмет." + name + " В пакете осталось место " + leftSpace);
        else System.out.println("Вы не можете положить предмет '" + name + "'. У вас не осталось свободного места ");
    }

}

