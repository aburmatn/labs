package lesson18;

public class UsernameReport extends Report {


    public UsernameReport(String name) {
        super(name);
    }

    @Override
    public String getReport() {
        System.out.println(updateCount(count) + " " +System.getProperty("user.name") + " : " + getName());
        return getName();
    }
}
