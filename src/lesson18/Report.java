package lesson18;

public class Report {

    private String name;
    static int count;

    public static int getCount() {
        return count;
    }

    public Report() {
    }

    Report(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int updateCount(int count){
        count = count+1;
        return count;
    }

    public String getReport(){
        count = count +1;
        System.out.println(count + " " + name);
        return name;
    }



}
