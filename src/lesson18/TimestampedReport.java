package lesson18;

import java.util.Date;

public class TimestampedReport extends Report {

    public TimestampedReport(String name) {
        super(name);
    }

    @Override
    public String getReport() {
        Date date = new Date(System.currentTimeMillis());
        System.out.println(updateCount(count) + " " + getName() + " " + date);
        return getName();
    }
}
