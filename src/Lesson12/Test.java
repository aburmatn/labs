package Lesson12;

public class Test {

    public static void main(String[] args) {

     Address address1 = new Address("Россия","Санкт-Петербург","13 Линия ВО", 14, 192020);
     Address address2 = new Address("Россия","null","null",0,0);


     Person person1 = new Person();
     person1.setName("Сергей");
     person1.setAge(35);
     person1.setWorkAddress(address1);

     Person person2 = new Person();
     person2.setName("Михаил");
     person2.setAge(45);
     person2.setWorkAddress(address1);

     Person person3 = new Person("Илья",45,address2);

        System.out.println("Информация о сотруднике :  " + person1.getName()+ " " + person1.getAge());
        person1.showAddressPublic(address1);

        System.out.println("Информация о сотруднике :  " + person2.getName()+ " " + person2.getAge());
        person2.showAddressPublic(address1);

        System.out.println("Информация о сотруднике :  " + person3.getName()+ " " + person3.getAge());
        person3.showAddressPublic(address2);

    }
}
