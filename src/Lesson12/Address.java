package Lesson12;

public class Address {

    String country;
    String  city;
    String street;
    int plz;
    int bld;


    Address( String country,String city){
        this.country=country;
        this.city = city;
    }

    Address( String country){
        this.country = country;
    }

    Address(String country, String city, String street, int bld, int plz){

        this(country,city);
        this.street = street;
        this.bld = bld;
        this.plz = plz;
    }


}
