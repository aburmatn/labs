package Lesson12;

public class Person {

    private String name;
    private int age;
    private Address workAddress;
    private Address homeAddresse;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Address getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(Address workAddress) {
        this.workAddress = workAddress;
    }

    public Address getHomeAddresse() {
        return homeAddresse;
    }

    public void setHomeAddresse(Address homeAddresse) {
        this.homeAddresse = homeAddresse;
    }

    Person(String name, int age, Address address){

        this.name = name;
        this.age = age;
        this.workAddress = address;
    }

    Person(){
    }


    public void showAddress() {
        if (workAddress.city.equalsIgnoreCase("null") && workAddress.street.equalsIgnoreCase("null") && workAddress.bld == 0){
            System.out.println(workAddress.country + " нет детальной информации о месте работы");}
        else
            System.out.println(workAddress.country + " " + workAddress.city + " " + workAddress.street + " " + workAddress.bld);
    }

    private void showAddress(Address workAddress){
        System.out.println(workAddress.country + " " + workAddress.city+ " " + workAddress.street + " " + workAddress.bld);
    }
    public void showAddressPublic(Address workAddress){
        showAddress();
    }

}
