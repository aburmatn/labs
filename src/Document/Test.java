package Document;

public class Test {

    public static void main(String[] args) {
        DocumentActivities Text1 = new TextDocument(200,"12.09.2010","Char",true);
        DocumentActivities Text2 = new TextDocument(147,"01.11.2012","Char",false);

        DocumentActivities File1 = new BinarDocument(20,"04.11.2012","Num",true);
        DocumentActivities File2 = new BinarDocument(15,"02.14.2020","Num",false);

        Text1.writeDocument();
        Text1.readDocument();
        ((TextDocument) Text1).run(((TextDocument) Text1).isExecutable(),((TextDocument) Text1).getDateOfCreation());

        Text2.readDocument();
        ((TextDocument) Text2).run(((TextDocument) Text2).isExecutable(),((TextDocument) Text2).getDateOfCreation());
        ((TextDocument) Text2).print(((TextDocument) Text2).isPrintable(),((TextDocument) Text2).getDateOfCreation());

        ((BinarDocument) File1).print(((BinarDocument) File1).isPrintable(),((BinarDocument) File1).getDateOfCreation());
        ((BinarDocument) File1).run(((BinarDocument) File1).isExecutable(),((BinarDocument) File1).getDateOfCreation());

        File2.readDocument();
        ((BinarDocument) File2).run(((BinarDocument) File2).isExecutable(),((BinarDocument) File2).getDateOfCreation());
        ((BinarDocument) File2).print(((BinarDocument) File2).isPrintable(),((BinarDocument) File2).getDateOfCreation());


    }
}

