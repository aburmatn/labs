package Document;


public abstract class Document {

    private int data;
    private String dateOfCreation;
    private String storageDataType;


    public Document(int data, String dateOfCreation, String storageDataType) {
        this.data = data;
        this.dateOfCreation = dateOfCreation;
        this.storageDataType = storageDataType;

    }


    public int getData() {
        return data;
    }


    public String getDateOfCreation() {
        return dateOfCreation;
    }


    public abstract void run(boolean isExecutable,String dateOfCreation);

    public abstract void print(boolean isPrintable,String dateOfCreation);

}


