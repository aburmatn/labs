package Document;

public class TextDocument extends Document implements DocumentActivities{

    private boolean isExecutable;
    private final boolean printable = true;


    public TextDocument(int data, String dateOfCreation, String storageDataType, boolean isExecutable) {
        super(data, dateOfCreation, storageDataType);
        this.isExecutable = isExecutable;
    }

    public boolean isExecutable() {
        return isExecutable;
    }

    public boolean isPrintable() {
        return printable;
    }

    @Override
    public void readDocument() {
        System.out.println("Чтение текстового с датой создания " + getDateOfCreation()+ " и размером " + getData() + " документа произведено.");
    }

    @Override
    public void writeDocument() {
        System.out.println("Запись текстового документа с датой создания " + getDateOfCreation()+  " и размером " + getData() +" zпроизведена.");
    }


    @Override
    public void run(boolean isExecutable,String dateOfCreation) {
        if (isExecutable == true)
        System.out.println("Запуск текстового документа с датой создания " + getDateOfCreation()+  " и размером " + getData() +" произведена.");
        else System.out.println("Данный тип файла не поддерживает функцию запуска");
    }

    @Override
    public void print(boolean printable,String dateOfCreation) {
        System.out.println("Печать текстового документа с датой создания " + dateOfCreation+" размера "+getData()+ " запущена.");

    }
}
