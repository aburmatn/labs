package Document;


public class BinarDocument extends Document implements DocumentActivities {

    private boolean isPrintable;
    private final boolean executable = true;

    public BinarDocument(int data, String dateOfCreation, String storageDataType, boolean isPrintable) {
        super(data, dateOfCreation, storageDataType);
        this.isPrintable = isPrintable;
    }

    public boolean isPrintable() {
        return isPrintable;
    }

    public boolean isExecutable() {
        return executable;
    }

    @Override
    public void readDocument() {
        System.out.println("Чтение двоичного документа c датой создания " + getDateOfCreation()+ " и размером " + getData() + " произведено.");

    }

    @Override
    public void writeDocument() {
        System.out.println("Запись двоичного документа c датой создания " + getDateOfCreation()+ " и размером " + getData() +" произведена.");

    }

    @Override
    public void run(boolean isExecutable,String dateOfCreation) {
        System.out.println("Двоичный документ с датой создания "+ getDateOfCreation()+" и размером " + getData()+ " запущен на выполнение");
    }

    @Override
    public void print(boolean isPrintable,String dateOfCreation) {
        if (isPrintable == true)
        System.out.println("Печать двоичного документа с датой создания "+ getDateOfCreation()+" и размером " + getData()+ " запущена");
        else System.out.println("Печать документа невозможна");
    }

}
