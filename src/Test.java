import Produkty.Produkt;
import Produkty.dary_lesa.Grib;
import Produkty.dary_lesa.Yagoda;
import Produkty.ovoschi.Ogurec;
import Produkty.ovoschi.Pomidor;
import banky.Banka;
import polzovateli.Polzovatel;
import shkafy.Shkaf;

public class Test {
    public static void main(String[] args) {
        Produkt grib1 = new Grib();
        Produkt yagoda2 = new Yagoda();
        Produkt ogurec3 = new Ogurec();
        Produkt pomidor3 = new Pomidor();
        Produkt pomidor4 = new Pomidor();

        Banka banka1 = new Banka(grib1);
        Banka banka2 = new Banka(yagoda2);
        Banka banka3 = new Banka(ogurec3);
        //Banka banka3 = new Banka(ogurec3, pomidor3);
        Banka banka4 = new Banka(pomidor4);
        banka3.takeProdukt();

        Polzovatel polzovatel1 = new Polzovatel("Vasya");

        Shkaf shkaf = new Shkaf(3);
        shkaf.takeBanka(banka3);
        shkaf.pullBanka(banka1);
        shkaf.pullBanka(banka2);
        shkaf.pullBanka(banka3);
        shkaf.pullBanka(banka4);
        shkaf.takeBanka(banka3);
        shkaf.pullBanka(banka4);
        // -------
        shkaf.takeBanka(banka1);
        polzovatel1.eat(banka1);
        shkaf.takeBanka(banka2);
        polzovatel1.eat(banka2);

    }
}
