package Lesson10_01;

public interface OvenActions {

    void turnHeatOn(int temperature);

    void turnHeatOff(int temperature);

    void decreaseTemperature(int decreaseTemperatureOn);

    void increaseTemperature(int increaseTemperatureOn);

}
