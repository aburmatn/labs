package Lesson10_01;

public class GasOven implements OvenActions {

    private int temperature;
    private int increaseTemperatureOn;
    private int decreaseTemperatureOn;
    Integer maxTemperature = new Integer(280);
    Integer minTemperature = new Integer(0);

    public GasOven(int temperature, int increaseTemperatureOn, int decreaseTemperatureOn) {
        this.temperature = temperature;
        this.increaseTemperatureOn = increaseTemperatureOn;
        this.decreaseTemperatureOn = decreaseTemperatureOn;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getIncreaseTemperatureOn() {
        return increaseTemperatureOn;
    }

    public int getDecreaseTemperatureOn() {
        return decreaseTemperatureOn;
    }

    @Override
    public void turnHeatOn(int temperature) {
        if (temperature != 0) {
            System.out.println("Газовая плита была включена. Нет необходимости во включении. Текущая температура "+temperature+ " градусов");
        } else System.out.println("Газовая плита включена");
    }



    @Override
    public void increaseTemperature(int increaseTemperatureOn) {
        if (maxTemperature>(increaseTemperatureOn + temperature))
        System.out.println("Температура газовой плиты была повышена на " + increaseTemperatureOn + " градусов");
        else System.out.println("Вы не можете повысить температуру больще, чем на "+ maxTemperature +" градусов");
    }

    @Override
    public void decreaseTemperature(int decreaseTemperatureOn) {
        if (minTemperature<decreaseTemperatureOn && decreaseTemperatureOn<(temperature + increaseTemperatureOn))
            System.out.println("Температура газовой была понижена на " + decreaseTemperatureOn + " градусов");
        else System.out.println("Введенное число выходит за границу допустимых значений. Пожалуйста, выберите температуру понижения от " + minTemperature + " до " + temperature);

    }

    @Override
    public void turnHeatOff(int temperature) {
        System.out.println("Газовая плита была выключена. Текущая температура "+ temperature + " градусов");
    }


}

