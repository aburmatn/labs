package Lesson10_01;

public class ElectricOven implements OvenActions {

    private int temperature;
    private int increaseTemperatureOn;
    private int decreaseTemperatureOn;
    Integer maxTemperature = new Integer(180);
    Integer minTemperature = new Integer(0);

    ElectricOven(int temperature, int increaseTemperatureOn, int decreaseTemperatureOn){
        this.temperature = temperature;
        this.increaseTemperatureOn = increaseTemperatureOn;
        this.decreaseTemperatureOn = decreaseTemperatureOn;
    }
    public int getIncreaseTemperatureOn() {
        return increaseTemperatureOn;
    }

    public int getDecreaseTemperatureOn() {
        return decreaseTemperatureOn;
    }


    public int getTemperature() {
        return temperature;
    }


    @Override
    public void turnHeatOn(int temperature) {
        if (temperature != 0) {
            System.out.println("Электрическая плита уже была включена");
        } else System.out.println("Электрическая плита включилась");
    }

    @Override
    public void increaseTemperature(int increaseTemperatureOn) {
        if (maxTemperature>(increaseTemperatureOn+temperature))
            System.out.println("Температура электроплиты была повышена на " + increaseTemperatureOn + " градусов");
        else System.out.println("Вы не можете повысить температуру электроплиты больще, чем на "+ maxTemperature +" градусов");
    }

    @Override
    public void decreaseTemperature(int decreaseTemperatureOn) {
        if (minTemperature<decreaseTemperatureOn && decreaseTemperatureOn<(temperature+increaseTemperatureOn))
            System.out.println("Температура электроплиты была понижена на " + decreaseTemperatureOn + " градусов");
        else System.out.println("Введенное число выходит за границу допустимых значений. Пожалуйста, выберите температуру понижения от " + minTemperature + " до " + (temperature+increaseTemperatureOn));

    }

    @Override
    public void turnHeatOff(int temperature) {
        {
            if (temperature != 0) {
                System.out.println("Электрическая плита выключена");
            } else System.out.println("Электрическая плита была выключена. Текущая температура "+ temperature + " градусов");
        }
    }
}
