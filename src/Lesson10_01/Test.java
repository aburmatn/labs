package Lesson10_01;

public class Test {

    public static void main(String[] args) {

        Integer temperature = new Integer(180);
        Integer increaseTemperatureOn = new Integer(99);
        Integer decreaseTemperatureOn = new Integer(200);

        Integer temperatureElOven = new Integer(0);
        Integer increaseTemperatureOnElOven = new Integer(179);
        Integer decreaseTemperatureOnElOven = new Integer(150);

        //Не знала, как лучше сделать, вынесла обертки для примитивов в тестовый класс.

        OvenActions gasOven = new GasOven(temperature,increaseTemperatureOn,decreaseTemperatureOn);
        OvenActions electricOven = new ElectricOven(temperatureElOven,increaseTemperatureOnElOven,decreaseTemperatureOnElOven);



        gasOven.turnHeatOn(((GasOven) gasOven).getTemperature());
        gasOven.increaseTemperature(((GasOven) gasOven).getIncreaseTemperatureOn());
        gasOven.decreaseTemperature(((GasOven) gasOven).getDecreaseTemperatureOn());
        gasOven.turnHeatOff(0);

        electricOven.turnHeatOn(((ElectricOven) electricOven).getTemperature());
        electricOven.increaseTemperature(((ElectricOven) electricOven).getIncreaseTemperatureOn());
        electricOven.decreaseTemperature(((ElectricOven) electricOven).getDecreaseTemperatureOn());
        electricOven.turnHeatOff(0);




    }

}
