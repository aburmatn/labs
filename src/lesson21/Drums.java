package lesson21;

public class Drums implements Playable {
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Drums(int size) {
        this.size = size;
    }

    private int size;

    @Override
    public void play() {
        System.out.println("Играет барабан размера " + size + " Текущая нота " + KEY);

    }

}
