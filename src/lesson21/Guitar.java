package lesson21;

public class Guitar implements Playable {

    private int stringCount;
//
//    public int getStringCount() {
//        return stringCount;
//    }
//
//    public void setStringCount(int stringCount) {
//        this.stringCount = stringCount;
//    }


    public Guitar(int stringCount) {
        this.stringCount = stringCount;
    }

    @Override
    public void play() {
        System.out.println("Играет гитара с количеством струн " + stringCount + " Текущая нота " + KEY);

    }
}
