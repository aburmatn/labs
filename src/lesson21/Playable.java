package lesson21;

public interface Playable {

    String KEY = "До Мажор";

    default void play(){
        System.out.println("Играет " + KEY);
    }

}
