package lesson21;

public class Trumpet implements Playable {

    public int getDiam() {
        return diam;
    }

    public void setDiam(int diam) {
        this.diam = diam;
    }

    public Trumpet(int diam) {
        this.diam = diam;
    }

    private int diam;

    @Override
    public void play() {
        System.out.println("Играет труба диаметром " + diam + ". Текущая нота " + KEY);
    }
}
