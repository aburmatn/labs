package lesson21;

public class Test {

    public static void main(String[] args) {
        Guitar guitar1 = new Guitar(6);
        Guitar guitar2 = new Guitar(4);
        Drums drums = new Drums(20);
        Trumpet trumpet1 = new Trumpet(14);
        Trumpet trumpet2 = new Trumpet(18);



        Playable[] instrumnets = new Playable[3];

        instrumnets[0] = guitar1;
        instrumnets[1] = drums;
        instrumnets[2] = trumpet2;

        for ( int i =0; i< instrumnets.length; i++){
            instrumnets[i].play();
        }
    }
}