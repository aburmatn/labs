package lesson13;
import stub.Simulator;
import static test.TestChain.startTestChain;

public class Environment {
    public static void main(String[] args) {
        System.out.println(Simulator.startSimulator());
        System.out.println(startTestChain());

    }
}
