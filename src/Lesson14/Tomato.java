package Lesson14;



public class Tomato extends Product implements Activities {

    @Override
    public void take(int weight, String date){

        if (date.equalsIgnoreCase("25.10.20")){
            System.out.println("Помидоры свежие. Возьми "+ weight+" грамм помидоров");
            Product.countWeight=countWeight + weight;
        } else{
        System.out.println("Помидоры испорчены. Вы не можете их использовать");
        Product.countWeight=0;
    }
    }

    @Override
    public void putBack(int weight, String date){
        System.out.println("Убери " + weight + " грамм обратно.");
        Product.countWeight=countWeight-weight;
    }



}