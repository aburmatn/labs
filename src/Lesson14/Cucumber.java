package Lesson14;

public class Cucumber extends Product implements Activities {

    @Override
    public void take(int weight, String date) {
        if (date.equalsIgnoreCase("25.10.20")) {
            System.out.println("Огурцы свежие. Возьми " + weight + " грамм помидоров");
            Product.countWeight = weight + countWeight;
        } else {
            System.out.println("Срок хранения огурцов - одна неделя. Огурцы испорчены. Вы не можете их использовать");
            Product.countWeight = 0;
        }
    }

    @Override
    public void putBack(int weight, String date){
        System.out.println("Убери " + weight + " грамм Огурцов обратно.");
        Product.countWeight=countWeight-weight;

    }
}