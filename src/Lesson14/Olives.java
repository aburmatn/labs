package Lesson14;


public class Olives extends Product implements Activities {


    @Override
    public void take(int weight,  String date ) {
        if (date.equalsIgnoreCase("20.10.20")) {
            System.out.println("Оливки свежие. Возьми " + weight + " грамм оливок");
            Product.countWeight = weight + countWeight;
        } else {
            System.out.println("Оливки испорчены. Вы не можете их использовать");
            Product.countWeight = 0;
        }
    }

    @Override
    public void putBack(int weight, String date){
        System.out.println("Убери " + weight + " грамм обратно.");
        Product.countWeight=countWeight-weight;
    }
}