package Lesson14;


public class Cheese extends Product implements Activities {

    @Override
    public void take(int weight, String date) {
        if (date.equalsIgnoreCase("23.10.20")) {
            System.out.println("Сыр свежий. Возьми " + weight + " грамм сыра");
            Product.countWeight = countWeight + weight;
        } else {
            System.out.println("Сыр испорчен. Вы не можете их использовать");
            Product.countWeight = 0;
        }
    }

    @Override
    public void putBack(int weight, String date){
        System.out.println("Убери " + weight + " грамм сыра обратно.");
        Product.countWeight=countWeight-weight;

    }
}